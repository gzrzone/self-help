package com.developer.selfhelp.ui.login

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.developer.selfhelp.api.ApiService
import com.developer.selfhelp.data.entities.UserInfo
import com.developer.selfhelp.data.repository.ApiRepository
import com.developer.selfhelp.data.response.Data
import com.developer.selfhelp.data.response.HistoryResponse

/**
 *
 * @desc 参考链接  https://medium.com/@chris.ribetti/android-viewmodel-livedata-repository-and-di-complete-and-super-quick-5a7d78fa7946
 * */

class LoginViewModel @ViewModelInject constructor(
    private val apiRepository: ApiRepository
): ViewModel() {

    val liveData: LiveData<List<UserInfo>> = apiRepository.liveData

    suspend fun getUserList() {
        apiRepository.getUserList()
    }

    val ldHistory: LiveData<HistoryResponse> = apiRepository.liveDataHistoryList

    suspend fun getHistoryList() {
        apiRepository.getHistoryList()
    }

}