package com.developer.selfhelp.ui.login

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.developer.selfhelp.R
import com.developer.selfhelp.base.BaseActivity
import com.developer.selfhelp.databinding.ActivityLoginBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LoginActivity : BaseActivity<ActivityLoginBinding>() {

    private val mViewModel: LoginViewModel by viewModels()
    lateinit var mAdapter: HistoryRecyclerViewAdapter

    override fun getLayoutId(): Int {
        return R.layout.activity_login
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding.rvDatas.layoutManager = LinearLayoutManager(this ,  LinearLayoutManager.VERTICAL , false)
        mBinding.rvDatas.addItemDecoration(DividerItemDecoration(this , DividerItemDecoration.VERTICAL))
        mViewModel.ldHistory.observe(this , Observer {
            Log.v("gzr" , "打印:${it.data.datas}")
            mAdapter = HistoryRecyclerViewAdapter(it.data.datas)
            //        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
            mBinding.rvDatas.adapter = mAdapter
//            tv_show_result.text = it.toString()
        })

        button_get.setOnClickListener {
            lifecycleScope.launch(Dispatchers.IO) {
                mViewModel.getHistoryList()
            }
        }
    }
}

