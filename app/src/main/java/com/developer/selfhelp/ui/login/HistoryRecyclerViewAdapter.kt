package com.developer.selfhelp.ui.login

import com.developer.selfhelp.R
import com.developer.selfhelp.base.MyBaseAdapter
import com.developer.selfhelp.base.MyViewHolder
import com.developer.selfhelp.base.SingleLayoutAdapter
import com.developer.selfhelp.data.response.DataX

class HistoryRecyclerViewAdapter(private val datas: List<DataX>) : SingleLayoutAdapter(R.layout.item_history_list) {

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
    }

    override fun getObjForPosition(position: Int): Any {
        return datas[position]
    }

    override fun getItemCount(): Int {
        return datas.size
    }
}