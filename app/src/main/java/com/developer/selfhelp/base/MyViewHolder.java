package com.developer.selfhelp.base;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.developer.selfhelp.BR;

public class MyViewHolder extends RecyclerView.ViewHolder {

    private final ViewDataBinding mBinding;

    public MyViewHolder(ViewDataBinding binding) {
        super(binding.getRoot());
        this.mBinding = binding;
    }

    public void bind(Object obj) {
        mBinding.setVariable(BR.datax , obj);
        mBinding.executePendingBindings();
    }

}
