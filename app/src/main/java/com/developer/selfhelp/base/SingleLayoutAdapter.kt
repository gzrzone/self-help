package com.developer.selfhelp.base

abstract class SingleLayoutAdapter(private val layoutId: Int) :
    MyBaseAdapter() {
    override fun getLayoutIdForPosition(position: Int): Int {
        return layoutId
    }
}