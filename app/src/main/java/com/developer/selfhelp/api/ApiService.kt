package com.developer.selfhelp.api

import com.developer.selfhelp.data.entities.ResultModel
import com.developer.selfhelp.data.entities.UserInfo
import com.developer.selfhelp.data.response.HistoryResponse
import retrofit2.http.GET

interface ApiService {

    @GET("api/getUserList")
    suspend fun getUserList(): List<UserInfo>

    @GET("api/getUserListResult")
    suspend fun getUserListResult(): ResultModel<UserInfo>

    @GET("list/408/1/json")
    suspend fun getHistoryData(): HistoryResponse

}

