package com.developer.selfhelp.data.repository

import androidx.lifecycle.MutableLiveData
import com.developer.selfhelp.api.ApiService
import com.developer.selfhelp.data.entities.UserInfo
import com.developer.selfhelp.data.response.Data
import com.developer.selfhelp.data.response.HistoryResponse
import javax.inject.Inject

class ApiRepository @Inject constructor(
    private val apiService: ApiService
) {

    val liveData = MutableLiveData<List<UserInfo>>()
    suspend fun getUserList() {
        liveData.postValue(apiService.getUserListResult().datas)
    }

    val liveDataHistoryList = MutableLiveData<HistoryResponse>()
    suspend fun getHistoryList() {
        liveDataHistoryList.postValue(apiService.getHistoryData())
    }

}
