package com.developer.selfhelp.data.entities;


import java.io.Serializable;
import java.util.List;


/**
 *
 * @desc 返回结果
 * */
public class ResultModel<T> implements Serializable {


    /**
     * 成功消息
     */
    public static final String SUCCESS_MSG = "操作成功！";
    /**
     * 成功消息
     */
    public static final String FAIL_MSG = "操作失败！";
    /**
     * 成功码
     */
    public static final int SUCCESS_CODE = 0;
    /**
     * 失败码
     */
    public static final int FAIL_CODE = -1;
    /**
     * 访问失败，token过期
     */
    public static final int TOKEN_ERROR_CODE = -2;

    private int code;
    private String msg;
    private T data;
    private List<T> datas;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    @Override
    public String toString() {
        return "ResultModel{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                ", datas=" + datas +
                '}';
    }
}














