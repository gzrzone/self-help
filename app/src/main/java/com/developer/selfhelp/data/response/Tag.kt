package com.developer.selfhelp.data.response

data class Tag(
    val name: String,
    val url: String
)