package com.developer.selfhelp.data.response

data class HistoryResponse(
    val data: Data,
    val errorCode: Int,
    val errorMsg: String
)